# HPC SIG

## Background

The members of openEuler HPC SIG has been deeply optimized for top applications in HPC meteorology, manufacturing, molecular dynamics and other fields, and has achieved good optimization results in terms of performance and accuracy. At the same time, HPC Smart Intelligence activities were carried out, and many college teachers and students participated in the migration of HPC applications to openEuler.

## Goals

Through the establishment of the openEuler HPC SIG, HPC developers and users are brought together. Let optimization of Top application  be quickly applied to the scientific research field, attract universities, government and enterprises to participate in the construction of HPC software ecology, absorb the ideas and ideas of scholars in different fields, build HPC SIG into a base in the HPC field, and jointly prosper the HPC+openEuler ecology.

## SIG 组职责

- Add HPC open-source software support for openEuler community
- Migrate, adapte and optimize HPC open-source software within openEuler,and output the corresponding migration and tuning documents
- Respond to user feedback in a timely manner and resolove the problems
- Manage the documentations, meetings, mailist and IRC within HPC SIG

### 交付物

- Source,patch and document

## 成员

### Maintainers 列表

- 王哲[@jerrywang125](https://gitee.com/jerrywang125), [32422240@qq.com](mailto:32422240@qq.com)

### committers 列表

- 何健聪[@hejiancong557](https://gitee.com/hejiancong557), [hejiancong1@huawei.com](mailto:hejiancong1@huawei.com)

- 覃璐瑶[@luyao201](https://gitee.com/luyao201), [tanluyao@huawei.com](mailto:tanluyao@huawei.com)

- 刘思圆[@liusiyuanOD](https://gitee.com/liusiyuanOD), [liusiyuan17@huawei.com](mailto:liusiyuan17@huawei.com)

- 汤国庆[@stephon-tone](https://gitee.com/stephon-tone), [tangguoqing8@huawei.com](mailto:tangguoqing8@huawei.com)

- 张嘉临[@zhangjialin9](https://gitee.com/zhangjialin9), [zhangjialin9@huawei.com](mailto:zhangjialin9@huawei.com)

- 杨德志[@deathy](https://gitee.com/deathy), [mapleyeh@qq.com](mailto:mapleyeh@qq.com)

- 王晓朋[@daniceexi](https://gitee.com/daniceexi), [wangxiaopeng99@huawei.com](mailto:wangxiaopeng99@huawei.com)

- 方春林[@iotwins](https://gitee.com/iotwins), [fangchunlin@huawei.com](mailto:fangchunlin@huawei.com)

## 联系方式

- [邮件列表](dev@openeuler.org)

## HPC Software List

- Lammps - [https://gitee.com/src-openeuler/lammps](https://gitee.com/src-openeuler/lammps)
